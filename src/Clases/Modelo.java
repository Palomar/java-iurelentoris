package Clases;

import java.util.List;

public class Modelo {
	
	private Integer id;
	private String nombredoc;
	private List<Campo> campos;
	
	public Modelo(Integer id, String nombredoc, List<Campo> campos){
		this.id = id;
		this.nombredoc = nombredoc;
		this.campos = campos;
	}
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}

	public String getNombredoc() {
		return nombredoc;
	}

	public void setNombredoc(String nombredoc) {
		this.nombredoc = nombredoc;
	}

	public List<Campo> getCampos() {
		return campos;
	}

	public void setCampos(List<Campo> campos) {
		this.campos = campos;
	}
	
	public String toString(){
		StringBuilder cadena = new StringBuilder();
		cadena.append("REF. DOCUMENTO: " +id+ " "+ nombredoc + "\n\n");
		for(Campo c : campos){
			cadena.append("REF. CAMPO: "+c.getId()+"\n");
			cadena.append("NOMBRE CAMPO: "+c.getNombre()+"\n");
			cadena.append("POSICI�N CAMPO: "+c.getPos()+"\n");
			cadena.append("CONTENIDO: "+c.getDescripcion()+"\n");
			cadena.append("\n");
		}
		
		return cadena.toString();
	}
	

}
