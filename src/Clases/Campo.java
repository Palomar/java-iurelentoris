package Clases;

public class Campo {

	private Integer id;
	private String nombre;
	private Integer pos;
	private String descripcion;
	
	public Campo(Integer id, String nombre, Integer pos, String descripcion){
		this.id = id;
		this.nombre = nombre;
		this.pos = pos;
		this.descripcion = descripcion;
	}
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPos() {
		return pos;
	}

	public void setPos(Integer pos) {
		this.pos = pos;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
