package Principal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;

import Clases.Campo;
import Clases.Modelo;

public class Principal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		MongoClient mongo = crearConexion();
		if (mongo != null) {
			MongoDatabase database = mongo.getDatabase("iure");
			
			//muestraModelos(database);
			
			//generaModelosPorDefecto(database);
			
			/*for(int it=0;it<2;it++){
				Integer id = autoId(database, "model");
				List<Campo> pepita = new ArrayList<Campo>();
				pepita.add(new Campo(1,"aa",2,"aa"));
				pepita.add(new Campo(2,"campo2",1,"blablbalbbla"));
				Modelo pepe = new Modelo(id,"pepe", pepita);
				insertModel(database, pepe);
			}*/
			
			
			deleteField(mongo, database,2,1);
			
			
			//deleteModel(database,2);
			//eliminaModelos(database);
			
			
			
			//muestraModelos(database);
			
			mongo.close();
		}else {
            System.out.println("Error: Conexi�n no establecida");
        }
	}
	
	public static void muestraModelos(MongoDatabase database){
		//Muestra los modelos de la mongodb
		MongoCollection<Document> collection = database
				.getCollection("model");
 
		List<Document> modelos = (List<Document>) collection.find().into(
				new ArrayList<Document>());
		
		List<Modelo> listaModelos = new ArrayList<Modelo>();
		
		for (Document modelo : modelos) {
			@SuppressWarnings("unchecked")
			List<Document> documentos = (List<Document>) modelo.get("documento");
			for (Document documento : documentos) {
				//Dentro de un documento
				Integer idDoc = documento.getInteger("id");
				String nombredocumento = documento.getString("nombredoc");
				@SuppressWarnings("unchecked")
				List<Document> campos = (List<Document>) documento.get("campos");
				List<Campo> listaCampos = new ArrayList<Campo>();
				for(Document campo : campos){
					Integer id = campo.getInteger("id");
					String nombre = campo.getString("nombre");
					Integer pos = campo.getInteger("pos");
					String desc = campo.getString("descripcion");
					listaCampos.add(new Campo(id, nombre, pos, desc));
					
				}
				//Reordeno los campos seg�n su posici�n
				listaCampos.sort(Comparator.comparing(Campo::getPos));
				Modelo documentoMongo = new Modelo(idDoc, nombredocumento, listaCampos);
				listaModelos.add(documentoMongo);
				//System.out.println(documentoMongo);
			}
		}
		System.out.print(listaModelos.toString());
	}
	
	public static void eliminaModelos(MongoDatabase database){
		//Elimina todos los modelos
		MongoCollection<Document> collection = database.getCollection("model");
		collection.deleteMany(new Document());
	}
	
	public static void generaModelosPorDefecto(MongoDatabase database){
		//Genera la colecci�n de modelos con el fichero modelos.json
		String ruta = new File("").getAbsolutePath();
		ruta += "/modelos"; //2.json
		database.getCollection("model").drop();
		
		for(int i=2;i<4;i++){
			String rutaModelo = ruta + i + ".json";
			String s;
			StringBuilder s2 = new StringBuilder();
			try (BufferedReader lector = new BufferedReader(new InputStreamReader(
	                new FileInputStream(rutaModelo), "UTF8"));) {
				while ((s=lector.readLine()) != null){
					s2.append(s);
				}
				Document doc = Document.parse(s2.toString());
				database.getCollection("model").insertOne(doc);
			}catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
		
	}
	
	public static void insertModel(MongoDatabase database, Modelo model){
		MongoCollection<Document> collection = database
				.getCollection("model");
		
		Document newDocument = new Document();
		List<Document> documentos = new ArrayList<Document>();
		
		Document document = new Document();
		document.put("id", model.getId());
		document.put("nombredoc", model.getNombredoc());
		List<Document> listaCampos = new ArrayList<Document>();
				
		for(int i=0;i<model.getCampos().size();i++){
			Document campo = new Document();
			campo.put("id", model.getCampos().get(i).getId());
			campo.put("nombre", model.getCampos().get(i).getNombre());
			campo.put("pos", model.getCampos().get(i).getPos());
			campo.put("descripcion", model.getCampos().get(i).getDescripcion());
			listaCampos.add(campo);
		}
		document.put("campos", listaCampos);
		documentos.add(document);
		newDocument.put("documento",documentos);
		collection.insertOne(newDocument);
	}
	
	public static Integer autoId(MongoDatabase database, String col){
		//Genera una Id autom�tica para la colecci�n que se desee
		MongoCollection<Document> collection = database
				.getCollection(col);
		List<Document> modelos = (List<Document>) collection.find().into(
				new ArrayList<Document>());
		Integer id = 0;
		for (Document modelo : modelos) {
			List<Document> documentos = (List<Document>) modelo.get("documento");
			for (Document documento : documentos) {
				//Dentro de un documento
				if (documento.getInteger("id") > id){
					id = documento.getInteger("id");
				}
			}
		}
		id++;
		return id;
	}
	
	public static void deleteModel(MongoDatabase database, Integer id){
		//Elimina el modelo seg�n la ID proporcionada
		MongoCollection<Document> collection = database
				.getCollection("model");
		Bson condition = new Document("$eq", id);
		Bson filter = new Document("documento.id", condition);
		collection.deleteOne(filter);
	}
	
	public static void deleteField(MongoClient mongo, MongoDatabase database, Integer documento, Integer campo){
		MongoCollection<Document> collection = database
				.getCollection("model");
		DB db = (DB) mongo.getDB("iure");
		DBCollection coll = db.getCollection("model");
		
		///Bson conditionField = new Document("$eq", campo);
		//Bson fieldId = new Document("id",conditionField);
		///Bson conditionDocument = new Document("$eq",documento);
		
		//Bson filterDocumentId = new Document("id",conditionDocument);
		
		/*Bson filterField = new Document().append("campos.id", campo);
		Bson filterDocument = new Document().append("documento.id", documento);
		Bson operation = new Document("$pull",filterField);*/
		
		/*Bson filterDocument = Filters.eq("documento", new Document("id", documento));
		Bson delete = Updates.pull("documento", new Document("campos", new Document("id", campo)));
		
		System.out.println(filterDocument.toString());
		System.out.println(delete.toString());
		collection.updateOne(filterDocument, delete);*/
		
		//Sistema cl�sico que no funciona
		BasicDBObject query = new BasicDBObject().append("documento", new BasicDBObject("id",documento));
	    BasicDBObject update = new BasicDBObject().append("campos", new BasicDBObject("id",campo));
	    System.out.println(query.toJson());
	    System.out.println(update.toJson());
	    coll.update(query, new BasicDBObject("$pull", update));
	}
	
	private static MongoClient crearConexion(){
        MongoClient mongo = null;
        try{
        	//mongo = new MongoClient("localhost", 27017);
        	mongo = new MongoClient("192.168.1.39", 27017);
        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return mongo;
    }
	
	private static void printDatabases(MongoClient mongo) {
        @SuppressWarnings("deprecation")
		List<String> dbs = mongo.getDatabaseNames();
        for (String db : dbs) {
            System.out.println(" - " + db);
        }
    }

}
