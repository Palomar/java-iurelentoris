package Principal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;

import Clases.Campo;
import Clases.Modelo;

public class Principal3 {
/*
 * PARA MODELOS BASICOS
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		MongoClient mongo = crearConexion();
		if (mongo != null) {
			MongoDatabase database = mongo.getDatabase("iure");
			
			//System.out.println(muestraModelos(database, "model"));
			//System.out.println(muestraModelos(database,"model",2));
			
			//generaModelosPorDefecto(database);
			
			//Para insertar un modelo
			/*for(int it=0;it<2;it++){
				Integer id = autoId(database, "model");
				List<Campo> pepita = new ArrayList<Campo>();
				pepita.add(new Campo(1,"aa",2,"aa"));
				pepita.add(new Campo(2,"campo2",1,"blablbalbbla"));
				Modelo pepe = new Modelo(id,"pepe", pepita);
				insertModel(database, "model", pepe);
			}*/
			
			//Para insertar un campo en un modelo
			/*Campo nuevo = new Campo(autoIdField(database,"model",2),"camponuevo",4,"descripci�n random");
			insertField(database, "model",2, nuevo);*/
			
			//Para actualizar un modelo
			/*List<Campo> pepita = new ArrayList<Campo>();
			pepita.add(new Campo(1,"aa",2,"aa"));
			pepita.add(new Campo(2,"campo2",1,"blablbalbbla"));
			Modelo pepe = new Modelo(2,"pepe", pepita);
			updateModel(database, "model",pepe);*/
			
			//deleteField(database,"model",2,1);
			
			//deleteModel(database,"model",1);
			
			//autoId(database,"model");
			//autoIdField(database, "model",3);
			
			//eliminaModelos(database);
			
			
			
			//muestraModelos(database);
			
			mongo.close();
		}else {
            System.out.println("Error: Conexi�n no establecida");
        }
	}
	
	public static List<Modelo> muestraModelos(MongoDatabase database, String col){
		//Muestra los modelos de la mongodb
		MongoCollection<Document> collection = database
				.getCollection(col);
 
		List<Document> modelos = (List<Document>) collection.find().into(
				new ArrayList<Document>());
		
		List<Modelo> listaModelos = new ArrayList<Modelo>();
		
		for (Document modelo : modelos) {
			@SuppressWarnings("unchecked")
			//List<Document> documentos = (List<Document>) modelo.get("documento");
		
			Integer idDoc = modelo.getInteger("id");
			String nombredocumento = modelo.getString("nombredoc");
			@SuppressWarnings("unchecked")
			List<Document> campos = (List<Document>) modelo.get("campos");
			List<Campo> listaCampos = new ArrayList<Campo>();
			for(Document campo : campos){
				Integer id = campo.getInteger("id");
				String nombre = campo.getString("nombre");
				Integer pos = campo.getInteger("pos");
				String desc = campo.getString("descripcion");
				listaCampos.add(new Campo(id, nombre, pos, desc));
				
			}
			//Reordeno los campos seg�n su posici�n
			listaCampos.sort(Comparator.comparing(Campo::getPos));
			Modelo documentoMongo = new Modelo(idDoc, nombredocumento, listaCampos);
			listaModelos.add(documentoMongo);
			
		}
		return listaModelos;
	}
	
	public static Modelo muestraModelos(MongoDatabase database, String col, Integer documento){
		//Muestra un documento, servir� para retornar un modelo
		MongoCollection<Document> collection = database
				.getCollection(col);
		
		Bson condition = Filters.eq("id", documento);
		
		List<Document> modelos = (List<Document>) collection.find(condition).into(
				new ArrayList<Document>());
		Modelo documentoMongo = null;
		for (Document modelo : modelos) {
			Integer idDoc = modelo.getInteger("id");
			String nombredocumento = modelo.getString("nombredoc");
			@SuppressWarnings("unchecked")
			List<Document> campos = (List<Document>) modelo.get("campos");
			List<Campo> listaCampos = new ArrayList<Campo>();
			for(Document campo : campos){
				Integer id = campo.getInteger("id");
				String nombre = campo.getString("nombre");
				Integer pos = campo.getInteger("pos");
				String desc = campo.getString("descripcion");
				listaCampos.add(new Campo(id, nombre, pos, desc));
				
			}
			//Reordeno los campos seg�n su posici�n
			listaCampos.sort(Comparator.comparing(Campo::getPos));
			documentoMongo = new Modelo(idDoc, nombredocumento, listaCampos);
			//System.out.println(documentoMongo.toString());
			
		}
		return documentoMongo;
	}
	
	public static void eliminaModelos(MongoDatabase database, String col){
		//Elimina todos los modelos
		MongoCollection<Document> collection = database.getCollection(col);
		collection.deleteMany(new Document());
	}
	
	public static void generaModelosPorDefecto(MongoDatabase database){
		//Genera la colecci�n de modelos con el fichero modelos.json
		String ruta = new File("").getAbsolutePath();
		ruta += "/modelobasico"; //2.json
		database.getCollection("model").drop();
		
		for(int i=2;i<4;i++){
			String rutaModelo = ruta + i + ".json";
			String s;
			StringBuilder s2 = new StringBuilder();
			try (BufferedReader lector = new BufferedReader(new InputStreamReader(
	                new FileInputStream(rutaModelo), "UTF8"));) {
				while ((s=lector.readLine()) != null){
					s2.append(s);
				}
				Document doc = Document.parse(s2.toString());
				database.getCollection("model").insertOne(doc);
			}catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
		
	}
	
	public static void insertModel(MongoDatabase database, String col, Modelo model){
		MongoCollection<Document> collection = database
				.getCollection(col);
		
		//Document newDocument = new Document();
		//List<Document> documentos = new ArrayList<Document>();
		
		Document document = new Document();
		document.put("id", model.getId());
		document.put("nombredoc", model.getNombredoc());
		List<Document> listaCampos = new ArrayList<Document>();
				
		for(int i=0;i<model.getCampos().size();i++){
			Document campo = new Document();
			campo.put("id", model.getCampos().get(i).getId());
			campo.put("nombre", model.getCampos().get(i).getNombre());
			campo.put("pos", model.getCampos().get(i).getPos());
			campo.put("descripcion", model.getCampos().get(i).getDescripcion());
			listaCampos.add(campo);
		}
		document.put("campos", listaCampos);
		//documentos.add(document);
		//newDocument.put("documento",documentos);
		collection.insertOne(document);
	}
	
	public static void insertField(MongoDatabase database, String col, Integer documento, Campo campo){
		MongoCollection<Document> collection = database
				.getCollection(col);
		Document campoDoc = new Document();
		campoDoc.put("id", campo.getId());
		campoDoc.put("nombre",campo.getNombre());
		campoDoc.put("pos", campo.getPos());
		campoDoc.put("descripcion", campo.getDescripcion());
		
		Bson filterDocument = Filters.eq("id", documento);
		Bson insert = Updates.push("campos", campoDoc);
		
		collection.updateOne(filterDocument, insert);
	}
	
	public static Integer autoId(MongoDatabase database, String col){
		//Genera una Id autom�tica para la colecci�n que se desee
		MongoCollection<Document> collection = database
				.getCollection(col);
		List<Document> modelos = (List<Document>) collection.find().into(
				new ArrayList<Document>());
		Integer id = 0;
		for (Document modelo : modelos) {
			if (modelo.getInteger("id") > id){
				id = modelo.getInteger("id");
			}
		}
		id++;
		return id;
	}
	
	public static Integer autoIdField(MongoDatabase database, String col, Integer documento){
		//Genera una Id autom�tica para el campo de documento que se quiera
		MongoCollection<Document> collection = database
				.getCollection(col);
		
		Bson condition = Filters.eq("id", documento);
		
		List<Document> modelos = (List<Document>) collection.find(condition).into(
				new ArrayList<Document>());
		Integer id = 0;
		for (Document modelo : modelos) {
			//dentro del documento absorbe los campos del documento
			List<Document> listaCampos = (List<Document>) modelo.get("campos");
			for (Document campo : listaCampos){
				if (campo.getInteger("id") > id){
					id = campo.getInteger("id");
				}
			}
			
		}
		id++;
		return id;
	}
	
	public static void updateModel(MongoDatabase database, String col, Modelo model){
		MongoCollection<Document> collection = database
				.getCollection(col);
	
		Document document = new Document();
		document.put("id", model.getId());
		document.put("nombredoc", model.getNombredoc());
		List<Document> listaCampos = new ArrayList<Document>();
				
		for(int i=0;i<model.getCampos().size();i++){
			Document campo = new Document();
			campo.put("id", model.getCampos().get(i).getId());
			campo.put("nombre", model.getCampos().get(i).getNombre());
			campo.put("pos", model.getCampos().get(i).getPos());
			campo.put("descripcion", model.getCampos().get(i).getDescripcion());
			listaCampos.add(campo);
		}
		document.put("campos", listaCampos);

		
		Bson filterDocument = Filters.eq("id", model.getId());
		Bson update = Updates.set("nombredoc",model.getNombredoc());
		collection.updateOne(filterDocument, update);
		update = Updates.set("campos",listaCampos);
		collection.updateOne(filterDocument, update);
		
	}
	
	public static void deleteModel(MongoDatabase database, String col, Integer id){
		//Elimina el modelo seg�n la ID proporcionada
		MongoCollection<Document> collection = database
				.getCollection(col);
		Bson condition = new Document("$eq", id);
		Bson filter = new Document("id", condition);
		collection.deleteOne(filter);
	}
	
	public static void deleteField(MongoDatabase database, String col, Integer documento, Integer campo){
		MongoCollection<Document> collection = database
				.getCollection(col);
		
		Bson filterDocument = Filters.eq("id", documento);
		Bson delete = Updates.pull("campos", new Document("id", campo));
		
		System.out.println(filterDocument.toString());
		System.out.println(delete.toString());
		collection.updateOne(filterDocument, delete);
		
	}
	
	private static MongoClient crearConexion(){
        MongoClient mongo = null;
        try{
        	//mongo = new MongoClient("localhost", 27017);
        	mongo = new MongoClient("192.168.1.39", 27017);
        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return mongo;
    }
	
	private static void printDatabases(MongoClient mongo) {
        @SuppressWarnings("deprecation")
		List<String> dbs = mongo.getDatabaseNames();
        for (String db : dbs) {
            System.out.println(" - " + db);
        }
    }

}
