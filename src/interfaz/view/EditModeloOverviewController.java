package interfaz.view;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import Clases.Campo;
import Clases.Modelo;
import interfaz.MainApp;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class EditModeloOverviewController {
	
	@FXML
    private TableView<Modelo> modeloTable;
    @FXML
    private TableColumn<Modelo, Integer> idColumn;
    @FXML
    private TableColumn<Modelo, String> nombredocColumn;
    
    @FXML
    private Label idLabel;
    @FXML
    private Label nombredocLabel;
    
    @FXML
    private TextField modeloField;
    
    @FXML
    private TextField consulta;

    // Reference to the main application.
    private MainApp mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public EditModeloOverviewController() {
    	
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    	
    	// Inicializa la tabla de modelos
     
    	idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
    	nombredocColumn.setCellValueFactory(new PropertyValueFactory<>("nombredoc"));
    	showModeloDetails(null);
    	//Escucha los cambios de cursor sobre la tabla de modelos
        modeloTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showModeloDetails(newValue));
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        modeloTable.setItems(mainApp.getModeloData());
        
    }
    
    /**
     * Rellena las etiquetas
     * 
     * @param model modelo de documento
     */
    private void showModeloDetails(Modelo model) {
        if (model != null) {
            // Rellena las etiquetas con el modelo.
            idLabel.setText(model.getId().toString());
            nombredocLabel.setText(model.getNombredoc());
        }else {
            //Si es nulo lo borra
            idLabel.setText("");
            nombredocLabel.setText("");
        }
    }
    
    
    public Integer tryParse(String text) {
    	try {
    		return Integer.parseInt(text);
    	}catch (NumberFormatException e) {
    		return null;
    	}
    }
    
    
    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (modeloField.getText() == null || modeloField.getText().length() == 0) {
            errorMessage += "�Introduce un nombre Modelo!\n"; 
        }
        
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Campo inv�lido");
            alert.setHeaderText("Por favor introduce datos v�lidos.");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    /**
     * Maneja la opci�n de insertar el modelo.
     */
    @FXML
    private void handleInsertar() {
    	if (isInputValid()) {
    		MongoClient mongo = mainApp.crearConexion();
        	MongoDatabase database = mongo.getDatabase(mainApp.getDb());
        	List<Campo> listCampos = new ArrayList<Campo>();
        	listCampos.add(new Campo(1,"",1,""));
        	Modelo varmodelo = new Modelo(autoId(database, "model"), modeloField.getText(), listCampos);
        	
        	insertModel(database, "model", varmodelo);
        	
        	mongo.close();
        	
        	Alert alert = new Alert(AlertType.INFORMATION);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Insertado");
            alert.setHeaderText("Modelo insertado");
            alert.setContentText("Has introducido un modelo nuevo.");

            alert.show();
        	//Refresco
            modeloTable.setItems(mainApp.getModeloData());
            //Reposiciona el cursor
            int index=modeloTable.getItems().size()-1;
            
            /*for(Modelo m : modeloTable.getItems()){
            	index++;
            }*/
            modeloTable.getSelectionModel().select(index);
            showModeloDetails(varmodelo);
            
        }
    }
    
    /**
     * Inserta modelo en la mongodb
     * 
     * @param database
     * @param col
     * @param model
     */
    public void insertModel(MongoDatabase database, String col, Modelo model){
		MongoCollection<Document> collection = database
				.getCollection(col);
		
		Document document = new Document();
		document.put("id", model.getId());
		document.put("nombredoc", model.getNombredoc());
		List<Document> listaCampos = new ArrayList<Document>();
				
		for(int i=0;i<model.getCampos().size();i++){
			Document campo = new Document();
			campo.put("id", model.getCampos().get(i).getId());
			campo.put("nombre", model.getCampos().get(i).getNombre());
			campo.put("pos", model.getCampos().get(i).getPos());
			campo.put("descripcion", model.getCampos().get(i).getDescripcion());
			listaCampos.add(campo);
		}
		document.put("campos", listaCampos);
		
		collection.insertOne(document);
	}
    
    public Integer autoId(MongoDatabase database, String col){
		//Genera una Id autom�tica para la colecci�n que se desee
		MongoCollection<Document> collection = database
				.getCollection(col);
		List<Document> modelos = (List<Document>) collection.find().into(
				new ArrayList<Document>());
		Integer id = 0;
		for (Document modelo : modelos) {
			if (modelo.getInteger("id") > id){
				id = modelo.getInteger("id");
			}
		}
		id++;
		return id;
	}
    
    
    /**
     * Maneja la opci�n de eliminar el modelo.
     */
    @FXML
    private void handleEliminar() {
    	int selectedModelo = modeloTable.getSelectionModel().getSelectedIndex();
    	
    	if (selectedModelo >= 0) {
    		MongoClient mongo = mainApp.crearConexion();
        	MongoDatabase database = mongo.getDatabase(mainApp.getDb());
        	
        	Modelo varmodelo = modeloTable.getSelectionModel().getSelectedItem();
        	
        	deleteModel(database, "model", varmodelo.getId());
        	
        	mongo.close();
        	
        	Alert alert = new Alert(AlertType.INFORMATION);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Eliminado");
            alert.setHeaderText("Modelo eliminado");
            alert.setContentText("Has eliminado el Modelo seleccionado.");

            alert.show();
        	//Refresco
            modeloTable.setItems(mainApp.getModeloData());
            
            showModeloDetails(null);
            
        } else {
            // Sin nada
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Sin selecci�n");
            alert.setHeaderText("No hay Modelo seleccionado");
            alert.setContentText("Por favor selecciona un Modelo de la lista.");

            alert.showAndWait();
        }
    }
    
    public void deleteModel(MongoDatabase database, String col, Integer id){
		//Elimina el modelo seg�n la ID proporcionada
		MongoCollection<Document> collection = database
				.getCollection(col);
		Bson condition = new Document("$eq", id);
		Bson filter = new Document("id", condition);
		collection.deleteOne(filter);
	}
    
    /**
     * Llamado por el bot�n Buscar.
     */
    @FXML
    private void handleConsulta() {
    	modeloTable.setItems(mainApp.getConsulta(consulta.getText()));
    }
    
    /**
     * Llamado por el bot�n Exporta.
     */
    @FXML
    private void handleExporta() {
    	//modeloTable.setItems(mainApp.getConsulta(consulta.getText()));
    	
    	int selectedModelo = modeloTable.getSelectionModel().getSelectedIndex();
    	
    	if (selectedModelo >= 0) {
    		//MongoClient mongo = mainApp.crearConexion();
        	//MongoDatabase database = mongo.getDatabase(mainApp.getDb());
        	
        	Modelo varmodelo = modeloTable.getSelectionModel().getSelectedItem();
        	//Se crea un modelo de documento que luego se formatear� en JSON
        	Document doc = new Document();
        	doc.put("id", varmodelo.getId());
        	doc.put("nombredoc", varmodelo.getNombredoc());
        	List<Document> campos = new ArrayList<Document>();
        	for(Campo c : varmodelo.getCampos()){
        		Document campo = new Document();
        		campo.put("id",c.getId());
        		campo.put("nombre", c.getNombre());
        		campo.put("pos", c.getPos());
        		campo.put("descripcion", c.getDescripcion());
        		campos.add(campo);
        	}
        	doc.put("campos", campos);
        	
        	String ruta = new File("").getAbsolutePath();
    		ruta += "/exportado.json";
    		
    		/*try (BufferedWriter bw=new BufferedWriter(new FileWriter(ruta));){
        		bw.write(doc.toJson());
        	}catch(IOException e){
        		System.out.println("Error: "+e);
        	}*/
    		
    		FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Guardar modelo");
            //System.out.println(pic.getId());
            fileChooser.getExtensionFilters().addAll(
                   // new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JSON", "*.json"),
                    new FileChooser.ExtensionFilter("JS", "*.js")
                );
            fileChooser.setInitialFileName("*.json");
            File file = fileChooser.showSaveDialog(mainApp.getPrimaryStage());
            
            if (file != null) {
               
            	try (BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(
        				new FileOutputStream(file.getAbsolutePath()),"UTF8"));){
            		bw.write(doc.toJson());
            		
            		
            		Alert alert = new Alert(AlertType.INFORMATION);
                    alert.initOwner(mainApp.getPrimaryStage());
                    alert.setTitle("Exportado");
                    alert.setHeaderText("Modelo exportado");
                    alert.setContentText("Has exportado un modelo.");
                    alert.show();
            	}catch(IOException e){
            		System.out.println("Error: "+e);
            	}
            }
    		
            
    		
        	
        	//exportModel(database, "model", varmodelo.getId());
        	
    		//mongo.close();
    		
    	}else{
    		// Sin nada
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Sin selecci�n");
            alert.setHeaderText("No hay Modelo seleccionado");
            alert.setContentText("Por favor selecciona un Modelo de la lista.");

            alert.showAndWait();
    	}
    }
    
    /**
     * Selecciona el modelo por parametro enviado para extraerlo de Mongo y generar un JSON
     * NO SE USAR� POR AHORA y est� adem�s incompleto
     * 
     * @param database
     * @param col
     * @param id
     */
    private Modelo exportModel(MongoDatabase database, String col, Integer documento){
		//Elimina el modelo seg�n la ID proporcionada
		MongoCollection<Document> collection = database
				.getCollection(col);
		Bson condition = Filters.eq("id", documento);
		
		List<Document> modelos = (List<Document>) collection.find(condition).into(
				new ArrayList<Document>());
		Modelo documentoMongo = null;
		for (Document modelo : modelos) {
			Integer idDoc = modelo.getInteger("id");
			String nombredocumento = modelo.getString("nombredoc");
			@SuppressWarnings("unchecked")
			List<Document> campos = (List<Document>) modelo.get("campos");
			List<Campo> listaCampos = new ArrayList<Campo>();
			for(Document campo : campos){
				Integer id = campo.getInteger("id");
				String nombre = campo.getString("nombre");
				Integer pos = campo.getInteger("pos");
				String desc = campo.getString("descripcion");
				listaCampos.add(new Campo(id, nombre, pos, desc));
				
			}
			//Reordeno los campos seg�n su posici�n
			listaCampos.sort(Comparator.comparing(Campo::getPos));
			documentoMongo = new Modelo(idDoc, nombredocumento, listaCampos);
			//System.out.println(documentoMongo.toString());
			
		}
		return documentoMongo;
	}
}
