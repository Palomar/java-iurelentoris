package interfaz.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import Clases.Campo;
import Clases.Modelo;
import interfaz.MainApp;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class ModeloOverviewController {
	
	@FXML
    private TableView<Modelo> modeloTable;
    @FXML
    private TableColumn<Modelo, Integer> idColumn;
    @FXML
    private TableColumn<Modelo, String> nombredocColumn;
    
    @FXML
    private TableView<Campo> campoTable;
    @FXML
    private TableColumn<Campo, Integer> idcampoColumn;
    @FXML
    private TableColumn<Campo, String> nombrecampoColumn;
    @FXML
    private TableColumn<Campo, Integer> posColumn;
    @FXML
    private TableColumn<Campo, String> descripcionColumn;

    @FXML
    private Label idLabel;
    @FXML
    private Label nombredocLabel;
    
    @FXML
    private TextField idField;
    @FXML
    private TextField posField;
    @FXML
    private TextArea descripcionField;
    @FXML
    private TextField nombreField;
    
    

    // Reference to the main application.
    private MainApp mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public ModeloOverviewController() {
    	
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    	
    	// Inicializa la tabla de modelos
     
    	idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
    	nombredocColumn.setCellValueFactory(new PropertyValueFactory<>("nombredoc"));
    	
    	//Reseteo
    	showModeloDetails(null);
    	
    	//Escucha los cambios de cursor sobre la tabla de modelos
        modeloTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showModeloDetails(newValue));
        
        //Datos de campos
        idcampoColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
    	nombrecampoColumn.setCellValueFactory(new PropertyValueFactory<>("nombre"));
    	posColumn.setCellValueFactory(new PropertyValueFactory<>("pos"));
    	descripcionColumn.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
    	
    	//Resetea
    	showCampoDetails(null);
    	
    	//Escucha los cambios de cursor sobre los campos
        campoTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showCampoDetails(newValue));
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        modeloTable.setItems(mainApp.getModeloData());
        
    }
    
    /**
     * Fills all text fields to show details about the person.
     * If the specified person is null, all text fields are cleared.
     * 
     * @param model modelo de documento
     */
    private void showModeloDetails(Modelo model) {
        if (model != null) {
            // Rellena las etiquetas con el modelo.
            idLabel.setText(model.getId().toString());
            nombredocLabel.setText(model.getNombredoc());
            campoTable.setItems(mainApp.getCampoData(model.getId()));
          
        }else {
            //Si es nulo lo borra
            idLabel.setText("");
            nombredocLabel.setText("");
        }
    }
    
    /**
     * Muestra detalles de campo
     * 
     * @param campo campo de documento
     */
    private void showCampoDetails(Campo campo) {
    	if (campo != null) {
    		idField.setText(campo.getId().toString());
    		idField.setDisable(true);
    		nombreField.setText(campo.getNombre());
    		posField.setText(campo.getPos().toString());
    		descripcionField.setText(campo.getDescripcion());
    	}else {
            //Si es nulo lo borra
    		idField.setText("");
    		idField.setDisable(true);
    		nombreField.setText("");
    		posField.setText("");
    		descripcionField.setText("");
        }
    }
    
    /**
     * Maneja la opci�n de guardar del campo.
     */
    @FXML
    private void handleGuardar() {
    	int selectedModelo = modeloTable.getSelectionModel().getSelectedIndex();
    	int selectedCampo = campoTable.getSelectionModel().getSelectedIndex();
    	
    	if (selectedModelo >= 0 && selectedCampo >= 0) {
        	Campo varcampo = campoTable.getSelectionModel().getSelectedItem();
        	Modelo varmodelo = modeloTable.getSelectionModel().getSelectedItem();
        	if (isInputValid(varmodelo,varcampo)) {
        		guardar(varmodelo, varcampo);
            	MongoClient mongo = mainApp.crearConexion();
            	MongoDatabase database = mongo.getDatabase(mainApp.getDb());
            	updateModel(database,"model",varmodelo);
                mongo.close();
            	Alert alert = new Alert(AlertType.INFORMATION);
                alert.initOwner(mainApp.getPrimaryStage());
                alert.setTitle("Guardado");
                alert.setHeaderText("Campo guardado");
                alert.setContentText("Has guardado la modificaci�n del campo.");

                alert.show();// showAndWait();
            	//Actualizar la observable list
                modeloTable.setItems(mainApp.getModeloData());
                modeloTable.getSelectionModel().select(selectedModelo);
                campoTable.setItems(mainApp.getCampoData(varmodelo.getId()));
                int index=0;
                for(Campo c : campoTable.getItems()){
                	if(c.getId() == varcampo.getId()){
                    	campoTable.getSelectionModel().select(index);
                    }
                	index++;
                }
               
                
               // int index = campoTable.getSelectionModel().getFocusedIndex();
              //  campoTable.getFocusModel().focus();
                
                showCampoDetails(varcampo);
        	}
            
        } else {
            // Sin nada
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Sin selecci�n");
            alert.setHeaderText("No hay Modelo o Campo seleccionado");
            alert.setContentText("Por favor selecciona un Modelo y/o Campo de la lista.");

            alert.showAndWait();
        }
    }
    
    public void guardar(Modelo modelo, Campo campo){
    	
    	campo.setNombre(nombreField.getText());
    	campo.setPos(Integer.parseInt(posField.getText()));
    	campo.setDescripcion(descripcionField.getText());
    	
    	List<Campo> listaCamposModelo = modelo.getCampos();
    	int it=0;
    	for(Campo campoModelo : listaCamposModelo){
    		if(campoModelo.getId() == campo.getId()){
    			modelo.getCampos().set(it, campo);
    		}
    		it++;
    	}
    	
    }
    
    public void updateModel(MongoDatabase database, String col, Modelo model){
		MongoCollection<Document> collection = database
				.getCollection(col);
	
		Document document = new Document();
		document.put("id", model.getId());
		document.put("nombredoc", model.getNombredoc());
		List<Document> listaCampos = new ArrayList<Document>();
				
		for(int i=0;i<model.getCampos().size();i++){
			Document campo = new Document();
			campo.put("id", model.getCampos().get(i).getId());
			campo.put("nombre", model.getCampos().get(i).getNombre());
			campo.put("pos", model.getCampos().get(i).getPos());
			campo.put("descripcion", model.getCampos().get(i).getDescripcion());
			listaCampos.add(campo);
		}
		document.put("campos", listaCampos);

		
		Bson filterDocument = Filters.eq("id", model.getId());
		Bson update = Updates.set("nombredoc",model.getNombredoc());
		collection.updateOne(filterDocument, update);
		update = Updates.set("campos",listaCampos);
		collection.updateOne(filterDocument, update);
		
	}
    
    
    public Integer tryParse(String text) {
    	try {
    		return Integer.parseInt(text);
    	}catch (NumberFormatException e) {
    		return null;
    	}
    }
    
    public boolean posValid(Modelo modelo, Campo campo){
    	boolean retorna = true;
    	if(tryParse(posField.getText()) != null){
    		int pos = tryParse(posField.getText());
    		if (pos > 0){
    			List<Campo> listaCampos = modelo.getCampos();
        		for(Campo t_campo : listaCampos){
        			if(t_campo.getId() != campo.getId() && t_campo.getPos() == pos){
        				retorna = false;
        			}
        		}
    		}else{
    			retorna = false;
    		}
    		
    	}else{
    		retorna = false;
    	}
    	return retorna;
    }
    
    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     */
    private boolean isInputValid(Modelo modelo, Campo campo) {
        String errorMessage = "";

        if (posField.getText() == null || posField.getText().length() == 0) {
            errorMessage += "�Introduce una Posici�n!\n"; 
        }else if (tryParse(posField.getText()) == null ){
        	errorMessage += "�No introduciste un n�mero!\n";
        }else if(posValid(modelo, campo) == false){
        	errorMessage += "�No es una posici�n v�lida!\n";
        }
        
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Campo inv�lido");
            alert.setHeaderText("Por favor introduce datos v�lidos.");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    /**
     * Maneja la opci�n de insertar del campo.
     */
    @FXML
    private void handleInsertar() {
    	int selectedModelo = modeloTable.getSelectionModel().getSelectedIndex();
    	//int selectedCampo = campoTable.getSelectionModel().getSelectedIndex();
    	
    	if (selectedModelo >= 0) {
    		MongoClient mongo = mainApp.crearConexion();
        	MongoDatabase database = mongo.getDatabase(mainApp.getDb());
        	
        	Modelo varmodelo = modeloTable.getSelectionModel().getSelectedItem();
        	Campo varcampo = new Campo(autoIdField(database, "model", varmodelo.getId()), "", autoPosField(database, "model",varmodelo.getId()), "");
        	
        	insertField(database, "model", varmodelo.getId(), varcampo);
        	
        	mongo.close();
        	
        	Alert alert = new Alert(AlertType.INFORMATION);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Insertado");
            alert.setHeaderText("Campo insertado");
            alert.setContentText("Has introducido un campo nuevo.");

            alert.show();
        	//Refresco
            modeloTable.setItems(mainApp.getModeloData());
            modeloTable.getSelectionModel().select(selectedModelo);
            campoTable.setItems(mainApp.getCampoData(varmodelo.getId()));
            //Reposiciona el cursor
            int index=0;
            for(Campo c : campoTable.getItems()){
            	if(c.getId() == varcampo.getId()){
                	campoTable.getSelectionModel().select(index);
                }
            	index++;
            }
            showCampoDetails(varcampo);
            
        } else {
            // Sin nada
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Sin selecci�n");
            alert.setHeaderText("No hay Modelo seleccionado");
            alert.setContentText("Por favor selecciona un Modelo de la lista.");

            alert.showAndWait();
        }
    }
    
    
    public Integer autoIdField(MongoDatabase database, String col, Integer documento){
		//Genera una Id autom�tica para el campo de documento que se quiera
		MongoCollection<Document> collection = database
				.getCollection(col);
		
		Bson condition = Filters.eq("id", documento);
		
		List<Document> modelos = (List<Document>) collection.find(condition).into(
				new ArrayList<Document>());
		Integer id = 0;
		for (Document modelo : modelos) {
			//dentro del documento absorbe los campos del documento
			List<Document> listaCampos = (List<Document>) modelo.get("campos");
			for (Document campo : listaCampos){
				if (campo.getInteger("id") > id){
					id = campo.getInteger("id");
				}
			}
			
		}
		id++;
		return id;
	}
    
    public Integer autoPosField(MongoDatabase database, String col, Integer documento){
		//Genera una POS autom�tica para el campo de documento que se quiera
		MongoCollection<Document> collection = database
				.getCollection(col);
		
		Bson condition = Filters.eq("id", documento);
		
		List<Document> modelos = (List<Document>) collection.find(condition).into(
				new ArrayList<Document>());
		Integer pos = 0;
		for (Document modelo : modelos) {
			//dentro del documento absorbe los campos del documento
			List<Document> listaCampos = (List<Document>) modelo.get("campos");
			for (Document campo : listaCampos){
				if (campo.getInteger("pos") > pos){
					pos = campo.getInteger("pos");
				}
			}
			
		}
		pos++;
		return pos;
	}
    
    public void insertField(MongoDatabase database, String col, Integer documento, Campo campo){
		MongoCollection<Document> collection = database
				.getCollection(col);
		Document campoDoc = new Document();
		campoDoc.put("id", campo.getId());
		campoDoc.put("nombre",campo.getNombre());
		campoDoc.put("pos", campo.getPos());
		campoDoc.put("descripcion", campo.getDescripcion());
		
		Bson filterDocument = Filters.eq("id", documento);
		Bson insert = Updates.push("campos", campoDoc);
		
		collection.updateOne(filterDocument, insert);
	}
    
    /**
     * Maneja la opci�n de eliminar el campo.
     */
    @FXML
    private void handleEliminar() {
    	int selectedModelo = modeloTable.getSelectionModel().getSelectedIndex();
    	int selectedCampo = campoTable.getSelectionModel().getSelectedIndex();
    	
    	if (selectedModelo >= 0 && selectedCampo >= 0) {
    		MongoClient mongo = mainApp.crearConexion();
        	MongoDatabase database = mongo.getDatabase(mainApp.getDb());
        	
        	Modelo varmodelo = modeloTable.getSelectionModel().getSelectedItem();
        	Campo varcampo = campoTable.getSelectionModel().getSelectedItem();
        	
        	deleteField(database, "model", varmodelo.getId(), varcampo.getId());
        	
        	mongo.close();
        	
        	Alert alert = new Alert(AlertType.INFORMATION);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Eliminado");
            alert.setHeaderText("Campo eliminado");
            alert.setContentText("Has eliminado el campo seleccionado.");

            alert.show();
        	//Refresco
            modeloTable.setItems(mainApp.getModeloData());
            modeloTable.getSelectionModel().select(selectedModelo);
            campoTable.setItems(mainApp.getCampoData(varmodelo.getId()));
            //Reposiciona el cursor
            //campoTable.getSelectionModel().select(0);
            
            //showCampoDetails(varcampo);
            idField.setText("");
        	posField.setText("");
        	nombreField.setText("");
        	descripcionField.setText("");
            
        } else {
            // Sin nada
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Sin selecci�n");
            alert.setHeaderText("No hay Modelo y/o Campo seleccionado");
            alert.setContentText("Por favor selecciona un Modelo y/o Campo de la lista.");

            alert.showAndWait();
        }
    }
    
    public void deleteField(MongoDatabase database, String col, Integer documento, Integer campo){
		MongoCollection<Document> collection = database
				.getCollection(col);
		
		Bson filterDocument = Filters.eq("id", documento);
		Bson delete = Updates.pull("campos", new Document("id", campo));
		
		System.out.println(filterDocument.toString());
		System.out.println(delete.toString());
		collection.updateOne(filterDocument, delete);
		
	}
}
