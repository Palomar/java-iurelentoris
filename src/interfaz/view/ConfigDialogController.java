package interfaz.view;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import interfaz.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ConfigDialogController {

	@FXML
    private TextField ipField;
    @FXML
    private TextField puertoField;
    @FXML
    private TextField dbField;
    
    private Stage dialogStage;
    private boolean okClicked = false;
    private MainApp mainApp;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        
    }
    
    /**
     * Configuraci�n
     * 
     * @param 
     */
    public void setConfig(String ip, Integer puerto, String db) {
        ipField.setText(ip);
        puertoField.setText(puerto.toString());
        dbField.setText(db);
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
        	String ruta = new File("").getAbsolutePath();
    		ruta += "/config.conf";
        	try (BufferedWriter bw=new BufferedWriter(new FileWriter(ruta));){
        		bw.write(ipField.getText());
        		bw.newLine();
        		bw.write(puertoField.getText());
        		bw.newLine();
        		bw.write(dbField.getText());
        	}catch(IOException e){
        		System.out.println("Error: "+e);
        	}
        	mainApp.setMongoConfig(ipField.getText(), Integer.parseInt(puertoField.getText()), dbField.getText());
            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
    	if (isInputValid()) {
    		dialogStage.close();
    	}
        
    }

    public static Integer tryParse(String text) {
    	  try {
    	    return Integer.parseInt(text);
    	  } catch (NumberFormatException e) {
    	    return null;
    	  }
    }
    
    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (ipField.getText() == null || ipField.getText().length() == 0) {
            errorMessage += "�Introduce una IP!\n"; 
        }
        
        if (puertoField.getText() == null || puertoField.getText().length() == 0){
        	errorMessage += "�Introduce un puerto!\n"; 
        }else if (tryParse(puertoField.getText()) == null ){
        	errorMessage += "�No introduciste un n�mero de puerto!\n";
        }
        
        if (dbField.getText() == null || dbField.getText().length() == 0) {
            errorMessage += "�Introduce una Base de Datos!\n"; 
        }
       
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Campo inv�lido");
            alert.setHeaderText("Por favor introduce datos v�lidos.");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    public void updateConfig(){
		//Actualiza la configuraci�n
		
	}
    
}
