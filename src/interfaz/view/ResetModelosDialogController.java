package interfaz.view;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

import Clases.Modelo;
import interfaz.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ResetModelosDialogController {

    private Stage dialogStage;
    private boolean okClicked = false;
    private MainApp mainApp;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        
    }
    
    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
    	MongoClient mongo = mainApp.crearConexion();
    	
    	if (mongo != null) {
			MongoDatabase database = mongo.getDatabase(mainApp.getDb());
			generaModelosPorDefecto(database);
			mongo.close();
			okClicked = true;
	        dialogStage.close();
		}else {
            System.err.println("Error: Conexi�n no establecida");
        }
    	
        
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
    	dialogStage.close();
    	
    }
    
    /**
     * Restaura los modelos de formulario por defecto
     * 
     * @param database
     */

    public static void generaModelosPorDefecto(MongoDatabase database){
		//Genera la colecci�n de modelos con el fichero modelos.json
    	
    	String ruta = new File("").getAbsolutePath();
		ruta += "/modelobasico"; //2.json
		database.getCollection("model").drop();
		
		for(int i=2;i<4;i++){
			String rutaModelo = ruta + i + ".json";
			String s;
			StringBuilder s2 = new StringBuilder();
			try (BufferedReader lector = new BufferedReader(new InputStreamReader(
	                new FileInputStream(rutaModelo), "UTF8"));) {
				while ((s=lector.readLine()) != null){
					s2.append(s);
				}
				Document doc = Document.parse(s2.toString());
				database.getCollection("model").insertOne(doc);
			}catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
		
	}
    
}
