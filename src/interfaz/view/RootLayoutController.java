package interfaz.view;

import interfaz.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class RootLayoutController {
	
	 // Reference to the main application
    private MainApp mainApp;

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Configuración MongoDB.
     */
    @FXML
    private void handleConfigMongo() {
        mainApp.showConfigEditDialog(mainApp.getIp(), mainApp.getPuerto(), mainApp.getDb());
    }
    
    /**
     * Crea los modelos por defecto.
     */
    @FXML
    private void handleResetModelos() {
    	boolean okClicked = mainApp.showResetModelosDialog();
    	if (okClicked) {
            //Actualizar la observable list
    		mainApp.showModeloOverview();
        }
    }

     /**
     * Mantenimiento Modelos.
     */
    @FXML
    private void handleMantenimientoModelo() {
        mainApp.showModeloOverview();
    }
    
    /**
     * Mantenimiento Modelos.
     */
    @FXML
    private void handleAltaBajaModelo() {
        mainApp.showEditModeloOverview();
    }

    /**
     * Abre el importador
     */
    @FXML
    private void handleImport() {
        mainApp.showImport();
    }
    
    /**
     * Abre el importador
     */
    @FXML
    private void handleJSON() {
        mainApp.showJSON();
    }
    
    
    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Iurelentoris");
        alert.setHeaderText("Acerca del proyecto");
        alert.setContentText("Desarrollado para M06-UF3 por el alumno Jordi Palomar.");

        alert.showAndWait();
    }

    /**
     * Closes the application.
     */
    @FXML
    private void handleExit() {
        System.exit(0);
    }

}
