package interfaz;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import Clases.Campo;
import Clases.Modelo;
import interfaz.view.ConfigDialogController;
import interfaz.view.EditModeloOverviewController;
import interfaz.view.ModeloOverviewController;
import interfaz.view.ResetModelosDialogController;
import interfaz.view.RootLayoutController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainApp extends Application {
	
	private Stage primaryStage;
	private BorderPane rootLayout;
	
	private ObservableList<Modelo> modeloData = FXCollections.observableArrayList();
	private ObservableList<Campo> campoData = FXCollections.observableArrayList();
	
	private String ip;
	private Integer puerto;
	private String db;
	
	private Desktop desktop = Desktop.getDesktop();
	
	public MainApp() {
        // Add some sample data
        
    }
	
	/**
     * Retorna la lista de Modelos actualizada
     * @return
     */
    @SuppressWarnings("unchecked")
	public ObservableList<Modelo> getModeloData() {
    	MongoClient mongo = crearConexion();
		if (mongo != null) {
			MongoDatabase database = mongo.getDatabase(db);
			List<Modelo> q = muestraModelos(database, "model");
			modeloData.setAll(q);
			mongo.close();
		}else {
            System.err.println("Error: Conexi�n no establecida");
        }
    	
		return modeloData;
    }
    
    /**
     * Retorna la lista de Campos actualizada
     * @return
     */
    @SuppressWarnings("unchecked")
	public ObservableList<Campo> getCampoData(Integer idmodel) {
    	MongoClient mongo = crearConexion();
		if (mongo != null) {
			MongoDatabase database = mongo.getDatabase(db);
			List<Campo> q = muestraCampos(database, "model", idmodel);
			campoData.setAll(q);
			mongo.close();
		}else {
            System.err.println("Error: Conexi�n no establecida");
        }
    	
		return campoData;
    }
    
    /**
     * Retorna la lista de Modelos seg�n consulta
     * @return
     */
    @SuppressWarnings("unchecked")
	public ObservableList<Modelo> getConsulta(String cadena) {
    	MongoClient mongo = crearConexion();
		if (mongo != null) {
			MongoDatabase database = mongo.getDatabase(db);
			List<Modelo> q = muestraModelos(database, "model", cadena); // <-editar aqui
			modeloData.setAll(q);
			mongo.close();
		}else {
            System.err.println("Error: Conexi�n no establecida");
        }
    	
		return modeloData;
    }
    

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Iurelentoris");

        initRootLayout();
        showInitOverview();
        configuracionInicial();
       
	}
	
	/**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            
            // Give the controller access to the main app.
            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);
            
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Muestra la vista inicial.
     */
    public void showInitOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/InitOverview.fxml"));
            AnchorPane initOverview = (AnchorPane) loader.load();

            rootLayout.setCenter(initOverview);
        
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Muestra la edici�n de modelos.
     */
    public void showModeloOverview() {
        try {
   
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ModeloOverview.fxml"));
            AnchorPane modeloOverview = (AnchorPane) loader.load();

            rootLayout.setCenter(modeloOverview);
    
            ModeloOverviewController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Muestra el Alta y Baja de Modelos.
     */
    public void showEditModeloOverview() {
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/EditModeloOverview.fxml"));
            AnchorPane modeloOverview = (AnchorPane) loader.load();

            rootLayout.setCenter(modeloOverview);
            
      
            EditModeloOverviewController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Di�logo de configuraci�n para MongoDB
     * 
     * @param ip y puerto
     * @return true if the user clicked OK, false otherwise.
     */
    public boolean showConfigEditDialog(String ip, Integer puerto, String db) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ConfigDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Configuraci�n de Mongodb");
            
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ConfigDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            if(ip != null && puerto != null && db != null){
            	controller.setConfig(ip, puerto, db);
            }
            controller.setMainApp(this);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            
            
            
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Di�logo de reseteo de modelos
     * 
     * 
     * @return true if the user clicked OK, false otherwise.
     */
    public boolean showResetModelosDialog() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ResetModelosDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Restaurar modelos por defecto");
            
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ResetModelosDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
           
            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    
    /**
     * Para importar JSON
     * 
     * 
     * @return true if the user clicked OK, false otherwise.
     */
    public void showImport() {
        FileChooser fileChooser = new FileChooser();
        configureFileChooser(fileChooser);
		//fileChooser.setTitle("Open Resource File");
		File f = fileChooser.showOpenDialog(this.primaryStage);
		 
		if (f != null) {
			System.out.print("Importa desde: "+f.getAbsolutePath());
			MongoClient mongo = crearConexion();
	    	
	    	if (mongo != null) {
				MongoDatabase database = mongo.getDatabase(db);
				importaJSON(database, f.getAbsolutePath());
				mongo.close();
				
				Alert alert = new Alert(AlertType.INFORMATION);
	            alert.initOwner(getPrimaryStage());
	            alert.setTitle("Importado");
	            alert.setHeaderText("Modelo importado");
	            alert.setContentText("Has introducido un modelo nuevo import�ndolo.");

	            alert.show();
		        
			}else {
	            System.err.println("Error: Conexi�n no establecida");
	        }
        }
		
    }
    
    /**
     * Para visualizar JSON con la aplicaci�n predefinida
     * 
     * 
     * 
     */
    public void showJSON() {
        FileChooser fileChooser = new FileChooser();
        configureFileChooser(fileChooser);
		fileChooser.setTitle("Abrir JSON con aplicaci�n predeterminada");
		File f = fileChooser.showOpenDialog(this.primaryStage);
		 
		if (f != null) {
            openFile(f);
        }
		
    }
    
    //Abre el archivo con el programa adecuado
    private void openFile(File file) {
        try {
            desktop.open(file);
        } catch (IOException ex) {
            Logger.getLogger(
                MainApp.class.getName()).log(
                    Level.SEVERE, null, ex
                );
        }
    }
    
    //Configura la clase FileChooser para ponerle filtros entre otros
    private static void configureFileChooser(
            final FileChooser fileChooser) {      
                fileChooser.setTitle("Selecciona archivo a importar");
                fileChooser.setInitialDirectory(
                    new File(System.getProperty("user.home"))
                ); //Para definir una ruta por defecto del proyecto                 
                fileChooser.getExtensionFilters().addAll(
                   // new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JSON", "*.json"),
                    new FileChooser.ExtensionFilter("JS", "*.js")
                );
        }
    
    public List<Modelo> muestraModelos(MongoDatabase database, String col){
		//Muestra los modelos de la mongodb
		MongoCollection<Document> collection = database
				.getCollection(col);
 
		List<Document> modelos = (List<Document>) collection.find().into(
				new ArrayList<Document>());
		
		List<Modelo> listaModelos = new ArrayList<Modelo>();
		
		for (Document modelo : modelos) {
			@SuppressWarnings("unchecked")
			
			Integer idDoc = modelo.getInteger("id");
			String nombredocumento = modelo.getString("nombredoc");
			@SuppressWarnings("unchecked")
			List<Document> campos = (List<Document>) modelo.get("campos");
			List<Campo> listaCampos = new ArrayList<Campo>();
			for(Document campo : campos){
				Integer id = campo.getInteger("id");
				String nombre = campo.getString("nombre");
				Integer pos = campo.getInteger("pos");
				String desc = campo.getString("descripcion");
				listaCampos.add(new Campo(id, nombre, pos, desc));
				
			}
			//Reordeno los campos seg�n su posici�n
			listaCampos.sort(Comparator.comparing(Campo::getPos));
			Modelo documentoMongo = new Modelo(idDoc, nombredocumento, listaCampos);
			listaModelos.add(documentoMongo);
			
		}
		return listaModelos;
	}
    
    //Utilizada para las consultas de texto en los modelos
    public static List<Modelo> muestraModelos(MongoDatabase database, String col, String cadena){
		//Muestra un documento, servir� para retornar un modelo
		MongoCollection<Document> collection = database
				.getCollection(col);
		
		Bson condition = Filters.regex("nombredoc", cadena,"i");
		
		
		
		List<Document> modelos = (List<Document>) collection.find(condition).into(
				new ArrayList<Document>());
		
		List<Modelo> listaModelos = new ArrayList<Modelo>();
		//Modelo documentoMongo = null;
		for (Document modelo : modelos) {
			Integer idDoc = modelo.getInteger("id");
			String nombredocumento = modelo.getString("nombredoc");
			@SuppressWarnings("unchecked")
			List<Document> campos = (List<Document>) modelo.get("campos");
			List<Campo> listaCampos = new ArrayList<Campo>();
			for(Document campo : campos){
				Integer id = campo.getInteger("id");
				String nombre = campo.getString("nombre");
				Integer pos = campo.getInteger("pos");
				String desc = campo.getString("descripcion");
				listaCampos.add(new Campo(id, nombre, pos, desc));
				
			}
			//Reordeno los campos seg�n su posici�n
			listaCampos.sort(Comparator.comparing(Campo::getPos));
			Modelo documentoMongo = new Modelo(idDoc, nombredocumento, listaCampos);
			listaModelos.add(documentoMongo);
			
		}
		return listaModelos;
	}
    
    public List<Campo> muestraCampos(MongoDatabase database, String col, Integer documento){
		//Muestra los modelos de la mongodb
		MongoCollection<Document> collection = database
				.getCollection(col);
		Bson condition = Filters.eq("id", documento);
		List<Document> modelos = (List<Document>) collection.find(condition).into(
				new ArrayList<Document>());
		
		List<Campo> listaCampos = new ArrayList<Campo>();
		
		for (Document modelo : modelos) {
			@SuppressWarnings("unchecked")
			List<Document> campos = (List<Document>) modelo.get("campos");
			
			for(Document campo : campos){
				Integer id = campo.getInteger("id");
				String nombre = campo.getString("nombre");
				Integer pos = campo.getInteger("pos");
				String desc = campo.getString("descripcion");
				listaCampos.add(new Campo(id, nombre, pos, desc));
			}
			//Reordeno los campos seg�n su posici�n
			listaCampos.sort(Comparator.comparing(Campo::getPos));
		}
		return listaCampos;
	}
    
    /**
     * Operaciones de importaci�n de JSON
     * 
     * @param database
     * @param url
     */
    private void importaJSON(MongoDatabase database, String url){
		//Importa un fichero json a la BBDD
    	
    	//String ruta = new File("").getAbsolutePath();
		//ruta += "/modelobasico"; //2.json
		
		String s;
		StringBuilder s2 = new StringBuilder();
		try (BufferedReader lector = new BufferedReader(new InputStreamReader(
                new FileInputStream(url), "UTF8"));) {
			while ((s=lector.readLine()) != null){
				s2.append(s);
			}
			Document doc = Document.parse(s2.toString());
			doc.replace("id", autoId(database, "model"));
			database.getCollection("model").insertOne(doc);
		}catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		
	}
    
    public Integer autoId(MongoDatabase database, String col){
		//Genera una Id autom�tica para la colecci�n que se desee
		MongoCollection<Document> collection = database
				.getCollection(col);
		List<Document> modelos = (List<Document>) collection.find().into(
				new ArrayList<Document>());
		Integer id = 0;
		for (Document modelo : modelos) {
			if (modelo.getInteger("id") > id){
				id = modelo.getInteger("id");
			}
		}
		id++;
		return id;
	}
    
    private void configuracionInicial(){
    	String ruta = new File("").getAbsolutePath();
		ruta += "/config.conf";
		
		File conf = new File(ruta);
		if(conf.exists()){
			System.out.println("Existe archivo de configuraci�n.");
			String s;
			int i=0;
			try (BufferedReader lector = new BufferedReader(new InputStreamReader(
	                new FileInputStream(ruta), "UTF8"));) {
				while ((s=lector.readLine()) != null){
					if (i==0){
						ip = s;
					}else if(i==1){
						puerto = Integer.parseInt(s);
					}else if(i==2){
						db = s;
					}
					i++;
				}
			}catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}else{
			System.out.println("No existe archivo de configuraci�n.");
			boolean okClicked = showConfigEditDialog(ip,puerto,db);
			if (okClicked) {
                //Actualizar la observable list
                
            }
		}
		/*
		*/
    }
    
    public void setMongoConfig(String ip, Integer puerto, String db){
    	this.ip = ip;
    	this.puerto = puerto;
    	this.db = db;
    }
    
    public String getIp(){
    	return ip;
    }
    
    public Integer getPuerto(){
    	return puerto;
    }
    
    public String getDb(){
    	return db;
    }
    
    public MongoClient crearConexion(){
        MongoClient mongo = null;
        try{
        	//mongo = new MongoClient("localhost", 27017);
        	//mongo = new MongoClient("192.168.1.39", 27017);
        	mongo = new MongoClient(ip, puerto);
        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return mongo;
    }
    
    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }


	public static void main(String[] args) {
		launch(args);
	}
	
	
}
