# Iure Lentoris, aplicación académica

Aplicación de escritorio desarrollada con JAVAFX y MongoDB para la materia M06-UF3 de CFGS-DAM. Su finalidad es la gestión de documentos jurídicos para facilitar el archivo de modelos para el operador jurídico.

En el fichero "LEEME.odt" se hallan detalles académicos del desarrollo del proyecto así como los pasos de instalación y uso.

Por razones de tiempo es un proyecto de Unidad Formativa, y por tanto no es un proyecto completo finalizado, pero sirve a modo de esqueleto para una aplicación mucho más compleja.
